FROM rails:5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs netcat
RUN mkdir /myapp
WORKDIR /myapp
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
ADD . /myapp
ADD start.sh /myapp
RUN chmod a+x start.sh
EXPOSE 3000
