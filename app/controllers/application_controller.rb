class ApplicationController < ActionController::API
	include Response
	include ExceptionHandler
	before_action :authenticate

	protected
	def authenticate
		if request.headers['token'].nil?
			json_response( { error: 'Please provide application token'} , :unauthorized)
		else
			@token = request.headers['token']
		end
	end
	
end
