class BugsController < ApplicationController
  before_action :set_bug, only: [:show]

  # GET /bug
  def index
    @bugs = Bug.where(application_token: @token)
    json_response(@bugs)
  end

  # POST /bugs
  def create
    @new_number =  $redis.incr(@token) 

    bug_data = bug_params.as_json
    bug_data['number'] = @new_number
    bug_data['application_token'] = @token

    @result = CreateBugsJob.perform_later(bug_data);

    json_response({number: @new_number}, :created)
  end

  # GET /bugs/:id
  def show
    json_response(@bug)
  end

  def search
    json_response(Bug.search(@token, search_params))
  end

  private

  def bug_params
    params.require(:bug).permit(:status, :priority, :comment,
                               state_attributes: [:os, :device, :memory, :storage])
  end

  def search_params
    params.permit(:number, :status, :priority, :comment)
  end

  def set_bug
    @bug = Bug.find_by!(number: params[:id], application_token: @token)
  end
end
