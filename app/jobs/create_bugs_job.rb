class CreateBugsJob < ApplicationJob
  queue_as :bugs

  def perform(bug)
   	Bug.create!(application_token: bug['application_token'], number: bug["number"], status: bug["status"], priority: bug["priority"], comment: bug["comment"],
   											 state_attributes: {device: bug["state_attributes"]["device"], os: bug["state_attributes"]["os"],
   											  memory: bug["state_attributes"]["memory"], storage: bug["state_attributes"]["storage"]})
  end

end
