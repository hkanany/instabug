class Bug < ApplicationRecord

	include Elasticsearch::Model
	include Elasticsearch::Model::Callbacks

	belongs_to :state

	accepts_nested_attributes_for :state

	# validations
  	validates_presence_of :application_token, :number, :status, :priority, :comment
	validates_uniqueness_of :number, scope: :application_token

	enum status: { 'new-bug' => 0, 'in-progress' => 1, 'closed' => 2 }
    enum priority: { 'minor' => 0, 'major' => 1, 'critical' => 2 }

	

  	# Index
	index_name [Rails.application.engine_name, Rails.env].join('_')

	# Mapping
	mapping do
		indexes :application_token, type: 'string'
		indexes :number, type: 'integer'
		indexes :status, type: 'string'
		indexes :priority, type: 'string'
		indexes :comment, type: 'string'
	end


	def self.search(token, options={})

	    definition = Elasticsearch::DSL::Search::Search.new do
              
	        query do
	          
	          	bool do

	          		must do
	                    term application_token: token
	                end
			        
                    if !options[:number].nil? && !options[:number].empty?
	                    must do
	                      term number:  options[:number]
				        end
				    end

				    if !options[:status].nil? && !options[:status].empty?
				        must do
	                      term status:  options[:status]
				        end
				    end

			        if !options[:priority].nil? && !options[:priority].empty?
				        must do
	                      term priority:  options[:priority]
				        end
				    end

			        if !options[:comment].nil? && !options[:comment].empty?
				        filter do
	                      term comment: options[:comment]
				        end
				    end

			        
			        
	          	end
	        end
	    end

	    Elasticsearch::Model.search(definition, [Bug])
	end

	
end