class State < ApplicationRecord
	has_one :bug, dependent: :destroy
	
	# validations
  	validates_presence_of :device, :os, :memory, :storage
end
