Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :bugs, except: [:edit, :update, :delete] 
  post 'search', to: 'bugs#search'

end
