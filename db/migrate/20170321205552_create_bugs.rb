class CreateBugs < ActiveRecord::Migration[5.0]
  def change
    create_table :bugs do |t|
      t.string :application_token
      t.integer :number
      t.integer :status, default: 0
      t.integer :priority, default: 0
      t.string :comment
      t.references :state, index: true, foreign_key: true

      t.timestamps
    end
  end
end
