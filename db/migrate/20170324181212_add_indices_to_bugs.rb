class AddIndicesToBugs < ActiveRecord::Migration[5.0]
  def change
  	add_index :bugs, :application_token, name: "bug_application_token_index",  using: :btree
    add_index :bugs, :number, name: "bug_number_index", using: :btree

    add_index :bugs, [:application_token, :number], name: "bug_composite_index", unique: true, using: :btree
  end
end
