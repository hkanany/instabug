FactoryGirl.define do
  factory :bug do
    application_token { "2222222" }
    number { Faker::Number.number(4) }
    comment { Faker::Lorem.word }
    status :closed
    priority :minor
    state
  end
end