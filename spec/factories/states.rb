FactoryGirl.define do
  factory :state do
    device { Faker::StarWars.character }
    os { Faker::StarWars.character }
    memory { Faker::Number.number(3)  }
    storage { Faker::Number.number(3)  }
  end
end