require 'rails_helper'

RSpec.describe 'Bugs API', type: :request do
  # initialize test data 
  let!(:state) { create(:state) }
  let!(:bugs) { create_list(:bug, 10, state_id: state.id) }
  let!(:number) { bugs.first.number }

  # Test suite for GET /bugs
  describe 'GET /bugs' do
    # make HTTP get request before each example
    headers = {
      'token' => "2222222"
    }
    # make HTTP get request before each example
    before { get '/bugs' ,  params: {}, headers: headers}

    it 'returns bugs' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /bugs/:id
  describe 'GET /bugs/:id' do
    headers = {
      'token' => "2222222"
    }
    before { get "/bugs/#{number}" ,  params: {}, headers: headers}

    context 'when the record exists' do
      it 'returns the bug' do
        expect(json).not_to be_empty
        expect(json['number']).to eq(number)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:number) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Bug/)
      end
    end
  end



end