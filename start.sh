#!/bin/bash
while ! nc -z db 3306; do sleep 3; done
rake db:create
rake db:migrate
rake sneakers:run  &
bundle exec rails s -p 3000 -b '0.0.0.0'